from django.urls import path
from .views import *
from . import views


# import domyślnych widoków na potrzeby logowania
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("", startpage_response, name="start"),
    path("main_page", views.PostList.as_view(), name="home"),
    path("post/new/", views.post_new, name='post_new'),
    path("signup", signup_view, name='signup'),
    path("<slug:slug>/", views.post_detail, name='post_detail'),
    path("postdel/<slug:slug>/", views.post_delete, name='post_delete'),
    path("postedit/<slug:slug>/", views.post_edit, name='post_edit'),
    path("about", aboutus_view, name='about'),
    path("contact", contact_view, name='contact'),

# ścieżki do widoków logowania/wylogowania
    path("login", auth_views.LoginView.as_view(), name="login"),
    path("logout", auth_views.LogoutView.as_view(), name="logout"),
    path("logout-done", logout_done)
]