from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import HttpResponse, Http404
from .models import *
from django.views import generic
from .forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied




# Create your views here.

def startpage_response(request):
    return render(request, "start.html")

def logout_done(request):
    return render(request, "logout-done.html")

def aboutus_view(request):
    return render(request, "about-us.html")

def contact_view(request):
    return render (request, "contact.html")

def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

@login_required()
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('home')
    else:
        form = PostForm()
    return render(request, 'post-new.html', {'form': form, })

@login_required()
def post_detail(request, slug):
    template_name = 'post-detail.html'
    post = get_object_or_404(Post, slug=slug)
    comments = post.comments.filter(active=True)
    new_comment = None
    #Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
            # return redirect('post_detail')
    else:
        comment_form = CommentForm()

    return render(request, template_name, {'post': post,
                                       'comments': comments,
                                       'new_comment': new_comment,
                                       'comment_form': comment_form})

def restrict_user_access(function):
    def wrap(request, *args, **kwargs):
        post = Post.objects.get(slug=kwargs['slug'])
        if post.author != request.user:
            raise PermissionDenied
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__= function.__name__
    return wrap

@restrict_user_access
@login_required()
def post_delete(request, slug):
    post = get_object_or_404(Post, slug=slug)
    form = PostForm(request.POST)
    if request.method == "POST":
        post.author = request.user
        post.delete()
        return redirect('home')
    return render(request, "post-delete.html", {"form": form, "slug": slug})

@restrict_user_access
@login_required()
def post_edit(request, slug):
    post = get_object_or_404(Post, slug=slug)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', slug=post.slug)
    else:
        form = PostForm(instance=post)
    return render(request, "post-edit.html", {"form": form, "slug": slug})

class PostList(generic.ListView):
    queryset = Post.objects.filter().order_by("-created_on")
    template_name = "main-page.html"
    paginate_by = 4

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PostList, self).dispatch(*args, **kwargs)






