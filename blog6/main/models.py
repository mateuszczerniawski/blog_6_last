from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

# Create your models here.

class Post(models.Model):
    title = models.CharField('Tytuł', max_length=100, unique=True)
    slug = models.SlugField('Odnośnik', default=slugify(title), unique=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_posts', verbose_name='Autor')
    updated_on = models.DateTimeField(auto_now=True)
    content = models.TextField('Treść')
    created_on = models.DateTimeField('Stworzony', auto_now_add=True)

    class Meta:
        ordering = ['-created_on']
        verbose_name = 'Wiadomość'
        verbose_name_plural = 'Wiadomości'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments', verbose_name='Komentarz')
    author = models.CharField(max_length=30, verbose_name='Autor')
    email = models.EmailField()
    content = models.TextField(verbose_name='Treść')
    created_on = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_on']
        verbose_name = 'Komentarz'
        verbose_name_plural = 'Komentarze'

    def __str__(self):
        return 'Comment {} by {}'.format(self.content, self.author)